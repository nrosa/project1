#!/afs/nd.edu/user14/csesoft/2017-fall/anaconda3/bin/python3.6
# -*- coding: UTF-8 -*-
#Noelle Rosa (nrosa) and Harry Gebremedhin (hgebreme)

import sys
import zmq
import time
import datetime

#Create a ZMQ socket to talk to the Server
context = zmq.Context()								# Contexts help manage any sockets that are created 
socket = context.socket(zmq.SUB)					# The Context (named context here) creates a socket

if len(sys.argv) > 1:
	port = sys.argv[1]
else:
	port = "62400"									#default port	


print("Listening to port ", port," for messages")
socket.connect("tcp://localhost:" +port)				# connects to the socket that the user will input
socket.setsockopt_string(zmq.SUBSCRIBE, '')				# we need to subscribe to all filters ('') because we do not specify any in our code

while True:																# infinite loop
	try:
		string = socket.recv().decode('ascii')			#recieve any input from the server	and decode any ascii values
		print(string)									#send anythign recieved to stdout
		print (datetime.datetime.now())					#print the current timestamp whenever a message is recieved

	except KeyboardInterrupt:
		print("Caught control-C\n")						#handle the interrupt and exit	
		sys.exit()



if __name__ == "__main__":
	print("Not implemented.")	



