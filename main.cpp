//Noelle Rosa(nrosa) and Harry Gebremedhin(hgebreme)
// Server.cpp
// Be able to send the contents of a reasonably sized file of ASCII text via the publish mechanism of ZMQ, prompt/ confirm before sending contents of a file, error checking, gracefully exit
#include <zmq.hpp>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <string>
#include <sstream>
#include <unistd.h>
#include <dirent.h>

using namespace std;

void mySignalHandler (int);
void usage();
int send1(string, zmq::socket_t);
void quit();

int main(int argc, char *argv[]){
	//Parse input arguments and check for a port
	string port;
	if (argc == 1){

		//Use default port number
		port = "62400";
		cout<<"Unspecified port number. We will use the default number: "<<port<<endl;

	}
	else if (argc == 2){
		//User has entered a port number
		port = argv[1];
		int portNum = stoi(port);
		if (portNum > 62499 || portNum < 62400){
			port = "62400";
			cout<<"Unassigned port number. The range is between 62400 and 62499. We will use the default number: "<<port<<endl;
		}

	}
	else{
		cout<<"Incorrect number of arguments. There should be 0 or 1 arguments after the executable."<<endl;
		exit(EXIT_FAILURE);
	}


	//Catch control-c via signal handler
	signal (SIGINT, mySignalHandler); // if SIGINT occurs, the handler of the code is invoked 
	// there are many types of signals you can react to
	
	//Set up ZMQ Socket as a Publisher on the right port
	zmq::context_t context (1);								//initialize the context
	zmq::socket_t publisher (context, ZMQ_PUB);				//initialize the socket

	string portString = "tcp://*:" + port;					//concatenate port string in case user enters a different port
	publisher.bind(portString);				//UNSURE-- CHECK DOCUMENT (USED TO BE TCP NOT LOCALHOST)
											//Bind the publisher to the specified port
	cout<<"Welcome to the Multi-Process Message Broadcast System"<<endl<<"\t Now publishing on port "<<port<<endl;	
	while (1){
		//wait for user input (scanf)
		string command;
		cin>>command;						//takes in first thing user eners
		if(command.compare("help") == 0){	//help message
			usage();					
		}
		else if(command== "send"){
			string next;
			//If it is a send command, try to read the file - make sure it succeeded and the content is good
			//if the user confirms with a YES, blast it out via the ZMQ

			cin>>next;						//if sedn is specified, there should be another string following

			string file = next;				//just here to take care of a renaming issue we had

			//Start the sending process
			DIR* directory = opendir(".");	//Open current directory to scan
			struct dirent *dp;				//initialize pointer to scan with
			while ((dp = readdir(directory))!= NULL){	//check until reach the end of directory
				if (file.compare(dp->d_name) ==0){		//check for presence of the specified file
					break;	
				}
			}	
			

			if (dp == NULL){							//if gotten all the way and no match
				cout<<"Error: There was no file named "<<file<<"."<<endl;
				continue;
			}

			string endMessage = "";						//Start with an empty string to concatenate file lines to
			string FileName = file;						
			string line;
			string response;
			ifstream myStream(FileName);				//need to create ifstream to look at contents of file
			int c = 0;
			zmq::message_t message(120);				//initialize the message to be sent

			if (myStream.is_open()){					//means there were no problems opening file
				while(getline(myStream, line) ){		//get line by line while possible
					c+= line.length();					//keep track of the length of the message in chars
					endMessage= endMessage+line;		//concatenate end message
					int length = line.length();
					for (int i=0; i<length; i++){
						int x = line[i];				//turn char into an int (ascii table)
						if (x < 32 || x > 126){
							cout<<"Error: file contains non-ASCII readable text"<<endl;
							goto loopEnd;				//need to break out of for, inner while, and outer while loops
						}
					}
				}
			}

			else {
				cout<<"Error: Wrong permissions. File cannot be opened."<<endl;
				goto loopEnd;
			}

			if (c>120){									//if file is too long
				cout<<"Error: The file you have chosen is too long to send as a message"<<endl;
				goto loopEnd;
			}

			snprintf((char *) message.data(), 120, "%s", endMessage.c_str());	//put the message into the struct that will be sent to the socket
			cout<<"The following message will be sent ("<<c<<" characters):"<<endl;
			cout<<endMessage<<endl;
			cout<<"Type YES to confirm: ";
			cin>>response;
			cout<<endl;

			if (response.compare("YES")==0){			//if verified
				publisher.send(message);				//send the message to the publisher socket that was initialized above
				cout<<"Message sent!"<<endl;			//print that is has been sent
				continue;
			}



		}//end of send statement


		else if(command== "quit"){
			quit();
		}
		else{
			cout<< "That is not one of the available commands! Try one of these: "<<endl;
			usage();
		}
	loopEnd: ;											//tag to get out to end of the loop when there is an error
			 
	}//end of while


}//end of main


//FUNCTIONS


void mySignalHandler(int sig){
	//There is not much to clean up because we are using C++  which has clean up mechanisms employed for when the program ends. 
	//Cleans up the ZMQ context and sockets as well as any memory allocated over the course of the program
	
	cout<<"Graceful exit!"<<endl; 
	exit(EXIT_SUCCESS);


}

void usage(){
	cout<<"The commands for MP-MBS are as follows: "<<endl;
	cout<<"\thelp\t\t\t This command"<<endl;
	cout<<"\tsend XXX\t\t Send the file XXX out to all subscribed clients "<<endl;
	cout<<"\tquit\t\t\t Exit this code"<<endl;
}

void quit(){
	cout<<"Thanks for using MP-MBS! Exiting... "<<endl;
	exit(EXIT_SUCCESS);
}
